﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

//active

$(document).ready(function () {

    $.ajax({
        url: "/Home/GetView",
        type: "GET",
        data: { "id": "" },
        success: function (data) {
            $('#main_panel').html(data);
        }, error: function (error) {
            alert("An error from server");
        }
    });

    $('.menu-item').click(function () {
        var data = $(this).data('value');
        $.ajax({
            url: "/Home/GetView",
            type: "GET",
            data: { "id": data },
            success: function (data) {
                $('#main_panel').html(data);
            }, error: function (error) {
                alert("An error from server");
            }
        });
    });
});