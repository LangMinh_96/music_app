﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Music_app.Models;

namespace Music_app.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetView(string id)
        {
            switch (id)
            {
                case "i":
                    return PartialView("_Index");
                case "n":
                    return PartialView("_New");
                case "n_m":
                    return PartialView("_New_music");
                case "g":
                    return PartialView("_Genres");
                case "c":
                    return PartialView("_Charts");
                case "p":
                    return PartialView("_Playlist");
                case "v":
                    return PartialView("_Video");
                default:
                    return PartialView("_Index");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
